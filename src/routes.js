import {Brands} from "./components/Brands/Brands";
import {Manufacturers} from "./components/Manufacturers/Manufacturers";
import {Categories} from "./components/Categories/Categories";
import {Products} from "./components/Products/Products";
import {ProductPreview} from "./components/Products/ProductPreview/ProductPreview"
import {ProductEdit} from "./components/Products/ProductEdit/ProductEdit";

export const routes = [
    { path: '/brands', name: 'Бренды', component: Brands },
    { path: '/manufacturers', name: 'Производители', component: Manufacturers },
    { path: '/categories', name: 'Категории', component: Categories },
    { path: '/catalog', exact: true, name: 'Каталог', component: Products },
    { path: '/catalog/:id/preview', exact: true, name: 'Просмотр товара', component: ProductPreview },
    { path: '/catalog/:id/edit', exact: true, name: 'Редактирование товара', component: ProductEdit },
];