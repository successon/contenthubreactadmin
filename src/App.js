import React, { Component } from 'react';
import logo from '../src/assets/img/Rectangle.png';
import './App.scss';
import 'primereact/resources/themes/nova-dark/theme.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';
import "primeflex/primeflex.css"
import {PanelMenu} from 'primereact/panelmenu';
import {Redirect, Route, Switch, Router} from 'react-router-dom';
import {createBrowserHistory} from "history";
import { MenuService } from "./service/menu.service";
import { routes } from "./routes";


export const history = createBrowserHistory();
    class App extends Component {

      constructor(props) {
        super(props);
        this.state = {
          nodes: null,
          visibleMainMenu: false
        }
        this.menu = new MenuService();
        this.navigationMenu = this.navigationMenu.bind(this);
      }

      componentDidMount() {
          if(history.location.pathname === '/dashboard') {
              this.setState({visibleMainMenu: true})
          }
          this.getTokenData("Admin", "1111").then(res => {});
          this.menu.setUnvisibleMenu(this.navigationMenu);
        this.setState({nodes: this.menu.getMenu()})
      }

        navigationMenu() {
            this.setState({visibleMainMenu: false})
        }

        getTokenData(login, password) {
            let obj = {
                username: login,
                password: password
            }
            return fetch('http://212.24.48.52:8080/content/auth', {
                method: 'POST',
                //credentials: 'include',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(obj),
            })
                .then((res) => {
                    if (res.status === 200) {
                        return res.json()
                    }
                    return Promise.reject();
                }).then(res => {
                    sessionStorage.setItem('tokenData', res.token);
                });
        }

      render() {
        return (
            <div className='simplediv'>
                <div className='navigation-menu'>
                    <img className='logo-image' src={logo} alt={''} onClick={() => {
                        history.push('/dashboard');
                        this.setState({visibleMainMenu: true})
                    }}/>
                    <hr/>
                </div>
                {this.state.visibleMainMenu?<div className='navigation-premenu'>
                    <PanelMenu model={this.state.nodes} />
                </div>:null}
                <div className='main-container'>
                    <Router history={history}>
                        <Switch>
                            {routes.map((route, idx) => {
                                return route.component ? (
                                    <Route
                                        key={idx}
                                        path={route.path}
                                        exact={route.exact}
                                        name={route.name}
                                        render={props => (
                                            <route.component {...props} />
                                        )} />
                                ) : (null);
                            })}
                            <Redirect from="/" to="/dashboard" />
                        </Switch>
                    </Router>
                </div>
            </div>
        )
      };
}

export default App;
