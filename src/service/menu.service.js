import {history} from "../App";

export class MenuService {

    unvisibleMenu = null;

    data = [
        {
            "key": "0",
            "label": "Классификация и Категоризация",
            'leaf': false,
            "items": [{
                "key": "0-0",
                "label": "Категоризация",
                'leaf': true,
                "path":'/categories',
                command: (event) => {
                    history.push('/categories');
                    this.unvisibleMenu();
                }
            },
                {
                    "key": "0-1",
                    "label": "Классификация",
                    'leaf': false,
                    "items": [
                        { "key": "0-1-0", "label": "Производители", 'leaf': true, command: (event) => {
                                history.push('/manufacturers');
                                this.unvisibleMenu();
                            } },
                        { "key": "0-1-1", "label": "Бренды", 'leaf': true, command: (event) => {
                                history.push('/brands');
                                this.unvisibleMenu();
                            } },
                        { "key": "0-1-2", "label": "Страны мира", 'leaf': true }
                    ]
                },
                {
                    "key": "0-2",
                    "label": "Дополнительный класификатор",
                    'leaf': true
                },]
        },
        {
            "key": "1",
            "label": "Номерклатура",
            'leaf': false,
            "items": [
                { "key": "1-0", "label": "Посмотреть", 'leaf': true, command: (event) => {
                        history.push('/catalog');
                        this.unvisibleMenu();
                    } },
                { "key": "1-1", "label": "Создать", 'leaf': true }]
        },
        {
            "key": "2",
            "label": "Информационные модели",
            'leaf': false,
            "items": [
                { "key": "2-0", "label": "Характеристики", 'leaf': true },
                { "key": "2-1", "label": "Список подстановок", 'leaf': true },
                { "key": "2-2", "label": "Единицы измерения", 'leaf': true },
                { "key": "2-3", "label": "Группы атрибутов", 'leaf': true }]
        },
        {
            "key": "3",
            "label": "Заказчики",
            'leaf': false,
            "items": [
                { "key": "3-0", "label": "Посмотреть/Создать", 'leaf': true },
                { "key": "3-1", "label": "Имформационные модели", 'leaf': false, "items": [
                        { "key": "3-1-0", "label": "Категоризация", 'leaf': true },
                        { "key": "3-1-1", "label": "Модели", 'leaf': true }]
                },
                { "key": "3-1", "label": "Каталог/Привязка моделей", 'leaf': true }]
        },
        {
            "key": "4",
            "label": "Экспорт",
            'leaf': false,
            "items": [
                { "key": "4-0", "label": "Экспортные шаблоны", 'leaf': true }]
        },
        {
            "key": "5",
            "label": "Админка",
            'leaf': false,
            "items": [
                { "key": "5-0", "label": "Пользователи", 'leaf': true },
                { "key": "5-1", "label": "Департаменты", 'leaf': true },
                { "key": "5-2", "label": "Роли", 'leaf': true },
                { "key": "5-3", "label": "Допуски", 'leaf': true }]
        },
    ];

    getMenu() {
        return this.data
    }

    setUnvisibleMenu(someFunc) {
        this.unvisibleMenu = someFunc;
    }
}