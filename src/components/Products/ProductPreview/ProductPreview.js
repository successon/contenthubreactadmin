import React, { Component } from 'react';
import {Toolbar} from 'primereact/toolbar';
import {BreadCrumb} from 'primereact/breadcrumb';
import '../../../scss/preview.scss';
import emptyImg from '../../../assets/img/EmptyImg.png'
import {DataTable} from 'primereact/datatable';
import {Column} from 'primereact/column';
import { productService } from "../../../service/product.service";
import {TabView,TabPanel} from 'primereact/tabview';
import {ScrollPanel} from 'primereact/scrollpanel';

const items = [
    { "label": "Каталог" },
    { "label": "Просмотр товара" }
]

export class ProductPreview extends Component {

    constructor(props) {
        super(props);
        this.state = {
            items: [],
            activeIndex: 0,
            item: {},
            expandedRows: []
        }
        this.headerTemplate = this.headerTemplate.bind(this);
    }

    componentDidMount() {
        const id = window.location.pathname.split('/')[2];
        productService.getItem(id).then(res => {
            console.log(res)
            this.setState({items: res.pageItems, item: res.pageItems[0]})
        });
    }

    headerTemplate(data) {
        return data.groupName;
    }
    footerTemplate(data, index) {
        return (
            <React.Fragment>
            </React.Fragment>
        );
    }

    render(){
        return (<div className='grid-card'>
            <Toolbar>
                <div className="p-toolbar-group-left">
                    <BreadCrumb model={items} home={{label: 'AAA', icon: 'pi pi-home'}} />
                </div>
            </Toolbar>

            <ScrollPanel>
                <div className="product-preview">
                    {this.state.items[0] ?
                        (<div className="product-details">
                            <div className='preview-container'>
                                <div className='image-grid'>
                                    <img src={this.state.item.baseImage ?'http://212.24.48.52/statics/' + this.state.item.baseImage : emptyImg}
                                         alt={'test'}/>
                                </div>
                                <div className="p-grid">
                                    <div className='full-name-product'><span>{this.state.item.fullName}</span></div>
                                    <div className='code-product'><span>Артикул: {this.state.item.article}</span></div>
                                    <div className='code-product'><span>Продукт ID: {this.state.item.productId}</span></div>
                                    <div className='code-product'><span>Продукт CODE: {this.state.item.code}</span></div>
                                </div>
                            </div>
                            <div className='preview-container'>
                                <div className='clascat-grid'>
                                    <Toolbar>
                                        <div className="p-toolbar-group-left">
                                            СВОЙСТВА ТОВАРА
                                        </div>
                                    </Toolbar>
                                    <div className='code-product'><span>Статус:<br/><span>{this.state.item.status}</span></span></div>
                                    <div className='code-product'><span>Тип товара:<br/><span>{this.state.item.typeProduct}</span></span></div>
                                    <div className='code-product'><span>Страна производитель:<br/><span>{this.state.item.countryName}</span></span></div>
                                    <div className='code-product'><span>Вендор:<br/><span>{this.state.item.manufacturerName}</span></span></div>
                                    <div className='code-product'><span>Бренд:<br/><span>{this.state.item.brandName}</span></span></div>
                                    <div className='code-product'><span>Семейство:<br/><span>{this.state.item.familyName}</span></span></div>
                                    <div className='code-product'><span>Серия:<br/><span>{this.state.item.seriesName}</span></span></div>
                                    <div className='code-product'><span>Модель:<br/><span>{this.state.item.modelName}</span></span></div>
                                    <div className='code-product'><span>Штрихкоды:<br/>{this.state.item.barcodes &&
                                    this.state.item.barcodes.map((item,index)=>
                                        <span>{item.barcode}</span>
                                    )
                                    }</span></div>
                                    <div className='code-product'><span>Категории:<br/>{this.state.item.categories &&
                                    this.state.item.categories.map((item,index)=>
                                        <span>{item.name + ' (' + item.code + ')'}</span>
                                    )
                                    }</span></div>
                                    <Toolbar>
                                        <div className="p-toolbar-group-left">
                                            СВОЙСТВА КОНТЕНТА
                                        </div>
                                    </Toolbar>
                                </div>
                                <div className="character-grid">
                                    <TabView activeIndex={this.state.activeIndex} onTabChange={(e) => this.setState({activeIndex: e.index})}>
                                        <TabPanel header="ХАРАКТЕРИСТИКИ ТОВАРА">
                                            <DataTable value={this.state.item.productCards.cards} rowGroupMode="subheader" sortField="groupName" sortOrder={0} groupField="groupName"
                                                       rowGroupHeaderTemplate={this.headerTemplate} rowGroupFooterTemplate={this.footerTemplate}
                                                       expandableRowGroups={true} expandedRows={this.state.expandedRows} onRowToggle={(e) => this.setState({expandedRows:e.data})}>
                                                <Column field="attribute.name" />
                                                <Column field="value" />
                                                <Column field="unit.name" />
                                            </DataTable>
                                        </TabPanel>
                                        <TabPanel header="АКСЕССУАРЫ">

                                        </TabPanel>
                                    </TabView>
                                </div>
                            </div>
                        </div>) : <div></div>}
                </div>
            </ScrollPanel>



        </div>)
    }
}