import React, { Component } from 'react';
import '../../../scss/preview.scss';
import {DataTable} from 'primereact/datatable';
import {Column} from 'primereact/column';
import {TabView,TabPanel} from 'primereact/tabview';
import {Button} from "primereact/button";


export class ProductContent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            activeIndex: 0
        }
    }

    imageTemplateBody(rowData) {
        return <div style={{width: '88px', height: '88px', display: 'flex', justifyContent: 'center', alignItems: 'center'}}>
            <img src={'http://212.24.48.52/statics/' + rowData.link}
                 alt={'test'} style={{maxWidth: '100%', maxHeight: '100%'}}/>
        </div>
    }
    otherTemplateBody(rowData) {
        return <a>Скачать</a>
    }

    render(){
        const {item} = this.props;
        return (<div className='grid-card'>
            <Button label='Добавить' className="p-button-success" style={{marginRight:'.25em', marginLeft:'2.5em'}} />
            <Button label='Удалить' className="p-button-danger"  />
            <TabView activeIndex={this.state.activeIndex} onTabChange={(e) => this.setState({activeIndex: e.index})}>
                <TabPanel header="ФОТО">
                    <DataTable value={item.contents.filter(item => item.type === 'IMAGE')} scrollable={true} scrollHeight='100%' >
                        <Column field="name" header='Имя' sortable />
                        <Column field="fullName" header='Полное имя' sortable />
                        <Column field="type" header='Тип' />
                        <Column header='Просмотр' body={this.imageTemplateBody} />
                    </DataTable>
                </TabPanel>
                <TabPanel header="МАРКЕТИНГОВОЕ ФОТО">
                    <DataTable value={item.contents.filter(item => item.type === 'MARKETING_IMAGE')} scrollable={true} scrollHeight='100%'>
                        <Column field="name" header='Имя' sortable />
                        <Column field="fullName" header='Полное имя' sortable />
                        <Column field="type" header='Тип' />
                        <Column header='Просмотр' body={this.imageTemplateBody} />
                    </DataTable>
                </TabPanel>
                <TabPanel header="СЕРТИФИКАТ">
                    <DataTable value={item.contents.filter(item => item.type === 'CERTIFICATE')} scrollable={true} scrollHeight='100%'>
                        <Column field="name" header='Имя' sortable />
                        <Column field="fullName" header='Полное имя' sortable />
                        <Column field="type" header='Тип' />
                        <Column header='Просмотр' body={this.otherTemplateBody} />
                    </DataTable>
                </TabPanel>
                <TabPanel header="ИНСТРУКЦИЯ">
                    <DataTable value={item.contents.filter(item => item.type === 'INSTRUCTION')} scrollable={true} scrollHeight='100%'>
                        <Column field="name" header='Имя' sortable />
                        <Column field="fullName" header='Полное имя' sortable />
                        <Column field="type" header='Тип' />
                        <Column header='Просмотр' body={this.otherTemplateBody} />
                    </DataTable>
                </TabPanel>
                <TabPanel header="ВИДЕО">
                    <DataTable value={item.contents.filter(item => item.type === 'VIDEO')} scrollable={true} scrollHeight='100%'>
                        <Column field="name" header='Имя' sortable />
                        <Column field="fullName" header='Полное имя' sortable />
                        <Column field="type" header='Тип' />
                        <Column header='Просмотр' body={this.imageTemplateBody} />
                    </DataTable>
                </TabPanel>
                <TabPanel header="ОБЗОРНОЕ ВИДЕО">
                    <DataTable value={item.contents.filter(item => item.type === 'VIEW360')} scrollable={true} scrollHeight='100%'>
                        <Column field="name" header='Имя' sortable />
                        <Column field="fullName" header='Полное имя' sortable />
                        <Column field="type" header='Тип' />
                        <Column header='Просмотр' body={this.imageTemplateBody} />
                    </DataTable>
                </TabPanel>
                <TabPanel header="HTML КОНТЕНТ">
                    <DataTable value={item.contents.filter(item => item.type === 'HTML_CONTENT')} scrollable={true} scrollHeight='100%'>
                        <Column field="name" header='Имя' sortable />
                        <Column field="fullName" header='Полное имя' sortable />
                        <Column field="type" header='Тип' />
                        <Column header='Просмотр' body={this.otherTemplateBody} />
                    </DataTable>
                </TabPanel>
                <TabPanel header="ПИКТОГРАММА">
                    <DataTable value={item.contents.filter(item => item.type === 'PICTOGRAM')} scrollable={true} scrollHeight='100%'>
                        <Column field="name" header='Имя' sortable />
                        <Column field="fullName" header='Полное имя' sortable />
                        <Column field="type" header='Тип' />
                        <Column header='Просмотр' body={this.otherTemplateBody} />
                    </DataTable>
                </TabPanel>
            </TabView>
        </div>)
    }
}