import React, { Component } from 'react';
import '../../../scss/preview.scss';
import {DataTable} from 'primereact/datatable';
import {Column} from 'primereact/column';
import {ContextMenu} from 'primereact/contextmenu';


export class ProductCard extends Component {

    constructor(props) {
        super(props);
        this.state = {
            expandedRows: [],
            selectedRow: null
        }
        this.menu = [
            {label: 'История изменений', command: () => console.log(this.state.selectedRow)},
            {label: 'Редактировать', command: () => console.log(this.state.selectedRow)},
            {label: 'Комментарий', command: () => console.log(this.state.selectedRow)},
            {label: 'Удалить', command: () => console.log(this.state.selectedRow)}
        ];
        this.headerTemplate = this.headerTemplate.bind(this);
    }


    headerTemplate(data) {
        return data.groupName;
    }
    footerTemplate(data, index) {
        return (
            <React.Fragment>
            </React.Fragment>
        );
    }

    render(){
        const {item} = this.props;
        return (<div className='grid-card'>
            <div className='edit-grid-card'>
                <div>Атрибуты: {item.productCards.all} / {item.productCards.fill}</div>
                <div>Обязательные: {item.productCards.requiredAll} / {item.productCards.requiredFill}</div>
                <div>Ключевые: {item.productCards.keyAll} / {item.productCards.keyFill}</div>
            </div>

            <ContextMenu model={this.menu} ref={el => this.cm = el} onHide={() => this.setState({selectedRow: null})}/>

            <DataTable value={item.productCards.cards} rowGroupMode="subheader" sortField="groupName" sortOrder={0} groupField="groupName"
                       rowGroupHeaderTemplate={this.headerTemplate} rowGroupFooterTemplate={this.footerTemplate}
                       scrollable={true} scrollHeight='90%'
                       contextMenuSelection={this.state.selectedRow} onContextMenuSelectionChange={e => this.setState({selectedRow: e.value})}
                       onContextMenu={e => this.cm.show(e.originalEvent)}
                       expandableRowGroups={true} expandedRows={this.state.expandedRows} onRowToggle={(e) => this.setState({expandedRows:e.data})}>
                <Column field="attribute.name" header='Атрибут' />
                <Column field="value" header='Значение' />
                <Column field="unit.name" header='Юнит' />
            </DataTable>
        </div>)
    }
}