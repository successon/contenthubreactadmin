import React, { Component } from 'react';
import {Button} from 'primereact/button';
import {Toolbar} from 'primereact/toolbar';
import {BreadCrumb} from 'primereact/breadcrumb';
import '../../../scss/editMain.scss';
import { productService } from "../../../service/product.service";
import { manufacturerService } from "../../../service/manufacturer.service";
import { brandService }  from "../../../service/brand.service";
import { countryService } from "../../../service/country.service";
import { ProductMain } from "./ProductMain";
import { ProductCard } from "./ProductCard";
import { ProductContent } from "./ProductContent";


const items = [
    { "label": "Каталог" },
    { "label": "Редактирование товара" }
]

export class ProductEdit extends Component {

    constructor(props) {
        super(props);
        this.state = {
            item: {},
            manufacturers: [],
            countries: [],
            brands: [],
            selectedLi: 'main'
        }
        this.onChangeMethod = this.onChangeMethod.bind(this);
    }

    componentDidMount() {
        const id = window.location.pathname.split('/')[2];
        productService.getItem(id).then(res => {
            console.log(res);
            this.setState({item: res.pageItems[0]})
        });
        manufacturerService.getList().then(res => {
            this.setState({manufacturers: res.pageItems})
        });
        countryService.getList().then(res => {
            this.setState({countries: res.pageItems})
        });
        brandService.getList().then(res => {
            this.setState({brands: res.pageItems})
        });
    }

    onChangeMethod(e, key) {
        let obj = Object.assign({}, this.state.item);
        obj[key] = e.value;
        this.setState({ item: obj })
    }

    render() {
        return (<div className='main-div-container'>
            <div className='navigation-add-menu'>
                <ul>
                    <li className={(this.state.selectedLi === 'main')? 'selected-li' : ''} onClick={(e) => {
                        this.setState({selectedLi: 'main'})
                    }}><i className="pi pi-cog"></i> Основные</li>
                    <li className={(this.state.selectedLi === 'content')? 'selected-li' : ''} onClick={(e) => {
                        this.setState({selectedLi: 'content'});
                    }}><i className="pi pi-images"></i> Контент</li>
                    <li className={(this.state.selectedLi === 'card')? 'selected-li' : ''} onClick={(e) => {
                        this.setState({selectedLi: 'card'})
                    }}><i className="pi pi-copy"></i> Карточка</li>
                    <li  className={(this.state.selectedLi === 'history')? 'selected-li' : ''} onClick={(e) => {
                        this.setState({selectedLi: 'history'})
                    }}><i className="pi pi-replay"></i> История изменений</li>
                </ul>
            </div>
            <div className='content-div-container'>
                <Toolbar>
                    <div className="p-toolbar-group-left">
                        <BreadCrumb model={items} home={{label: 'AAA', icon: 'pi pi-home'}}/>
                    </div>
                    <div className="p-toolbar-group-right">
                        <Button label='Отменить' className="p-button-danger" style={{marginRight:'.25em'}} />
                        <Button label='Сохранить' className="p-button-success" onClick={() => this.setState({visibleAdd: true})} />
                    </div>
                </Toolbar>
                {this.state.selectedLi === 'main' ?
                    <ProductMain
                        item={this.state.item}
                        brands={this.state.brands}
                        countries={this.state.countries}
                        manufacturers={this.state.manufacturers}
                        onChangeMethod={this.onChangeMethod}
                    /> : this.state.selectedLi === 'content'?
                        <ProductContent
                            item={this.state.item}
                        />:this.state.selectedLi === 'card'?
                            <ProductCard
                                item={this.state.item}
                            />:this.state.selectedLi === 'history'?
                                <div>
                                    history
                                </div>:<div></div>}
                <div className='edit-grid-footer'>
                    <div>
                        Дата создания:<br/>
                        <span>20.05.2020</span>
                    </div>
                    <div>
                        Создатель:<br/>
                        <span></span>
                    </div>
                    <div>
                        Дата редактирования:<br/>
                        <span></span>
                    </div>
                    <div>
                        Редактор:<br/>
                        <span></span>
                    </div>
                </div>
            </div>
        </div>)
    }
}