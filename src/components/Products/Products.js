import React, { Component } from 'react';
import {Button} from 'primereact/button';
import {Toolbar} from 'primereact/toolbar';
import {BreadCrumb} from 'primereact/breadcrumb';
import '../../scss/catalog.scss';
import emptyImg from '../../assets/img/EmptyImg.png'
import {DataTable} from 'primereact/datatable';
import {Column} from 'primereact/column';
import { productService } from "../../service/product.service";
import {MultiSelect} from 'primereact/multiselect';
import { DataView, DataViewLayoutOptions } from "primereact/dataview";
import { Panel } from 'primereact/panel';
import {Checkbox} from 'primereact/checkbox';
import {history} from "../../App";
import {SplitButton} from "primereact/splitbutton";

const items = [
    { "label": "Каталог" }
]

export class Products extends Component {

    constructor(props) {
        super(props);
        this.state = {
            items: [],
            totalRows: 0,
            selectedItems: [],
            item: {},
            selectedColumns: [],
            buttons: [
                {
                    label: 'Удалить',
                    command: (e) => {
                        console.log(e.originalEvent.currentTarget);
                    }
                },
                {
                    label: 'Результат экспорта',
                    command: (e) => {
                        console.log(e);
                    }
                },
                {
                    label: 'Клонировать',
                    command:(e) => {
                        console.log(e)
                    }
                }
            ],
            columns: [
                {field: 'fullName', header: 'Имя'},
                {field: 'comment', header: 'Коментарий'},
                {field: 'description', header: 'Описание'},
                {field: 'code', header: 'Код'},
            ],
            layout: 'grid',
            checked: {},
        }
        this.onColumnToggle = this.onColumnToggle.bind(this);
        this.itemTemplate = this.itemTemplate.bind(this);
    }

    componentDidMount() {
        productService.getList().then(res => {
            this.setState({items: res.pageItems, totalRows: res.totalRows})
        });
        this.setState({selectedColumns: this.state.columns.slice(0,3)});
    }


    imgTemplate(rowData, column) {
        let src;
        if (!rowData.baseImage) {
            src = emptyImg;
        } else {
            src = 'http://212.24.48.52/statics/' + rowData.baseImage;
        }
        return (
            <div className='main-container-image'>
                {this.state.layout === 'grid' ? <div className={'checked-box' + (this.state.checked.hasOwnProperty(rowData.id) ? ' visible-checked': '')}>

                    <Checkbox checked={this.state.checked.hasOwnProperty(rowData.id)} onChange={e => {
                        let item = Object.assign({}, this.state.checked);
                        item[rowData.id] = rowData;
                        if(e.checked) {
                            e.originalEvent.currentTarget.parentElement.classList.add('visible-checked')
                        } else {
                            e.originalEvent.currentTarget.parentElement.classList.remove('visible-checked');
                            delete item[rowData.id]
                        }
                        this.setState({checked: item})

                    }}/>
                </div> : null}
                <div className='container-image'>
                    <div>
                        <img src={src} alt={rowData.brand} style={{maxWidth: '100%', maxHeight: '250px'}}  />
                    </div>
                </div>
            </div>
        );
    }

    onColumnToggle(event) {
        let selectedColumns = event.value;
        let orderedSelectedColumns = this.state.columns.filter(col => selectedColumns.includes(col));
        this.setState({selectedColumns: orderedSelectedColumns});
    }




    renderListItem(car) {
        const columnComponents = this.state.selectedColumns.map(col=> {
            return <Column field={col.field} header={col.header} sortable />;
        });
        return (
            <DataTable value={this.state.items}
                      onRowDoubleClick={this.onSelect}
                      scrollable={true} scrollHeight='100%'
                      selection={this.state.selectedItems} onSelectionChange={e => this.setState({selectedItems: e.value})}>
                <Column selectionMode="multiple" style={{width:'20px'}} />
                <Column field="baseImage" header="Фото" body={this.imgTemplate} style={{textAlign:'center'}}/>
                {columnComponents}
            </DataTable>
        );
    }
    itemTemplate(product, layout, index) {
        const columnComponents = this.state.selectedColumns.map(col=> {
            return <Column field={col.field} header={col.header} sortable />;
        });
        if (!product) {
            return;
        }

        if (layout === 'list')
            return (
                <div style={{display:'flex'}} className={'p-col-12' + (this.state.checked.hasOwnProperty(product.id) ? ' checked-table-row': '')}>
                    <div style={{width:'2%'}}><Checkbox checked={this.state.checked.hasOwnProperty(product.id)} onChange={e => {
                        let item = Object.assign({}, this.state.checked);
                        item[product.id] = product;
                        if(e.checked) {
                            e.originalEvent.currentTarget.parentElement.parentElement.classList.add('checked-table-row')
                            //e.originalEvent.currentTarget.parentElement.classList.add('visible-checked')
                        } else {
                            e.originalEvent.currentTarget.parentElement.parentElement.classList.remove('checked-table-row')
                            //e.originalEvent.currentTarget.parentElement.classList.remove('visible-checked');
                            delete item[product.id]
                        }
                        this.setState({checked: item})

                    }} /></div>
                    <div style={{width:'5%'}}><span>{this.imgTemplate(product)}</span></div>
                    <div style={{width:'30%'}}><span className='text-table-div'>{product.fullName}</span></div>
                    <div style={{width:'30%'}}><span className='text-table-div'>{product.description}</span></div>
                    <div style={{width:'20%'}}><span className='text-table-div'>{product.article}</span></div>
                    <div style={{width:'10%'}}><span className='text-table-div'>{product.article}</span></div>
                    <div style={{width:'3%'}}><span className='text-table-div'>{<div>
                        <i className="pi pi-eye" style={{marginRight:'.25em'}} onClick={(e) => {
                            history.push('catalog/' + product.productId + '/preview')
                        }}></i>
                        <i className="pi pi-pencil" onClick={(e) => {
                            history.push('catalog/' + product.productId + '/edit')
                        }}></i>
                    </div>}</span></div>
                </div>
            );
        else if (layout === 'grid')
            return (
                <div style={{ padding: '.5em' }} className="p-col-12 p-md-3">
                    <Panel header={this.imgTemplate(product)}>
                        <div className="product-detail">
                            <p>{product.fullName}</p>
                            <span>{product.productId}</span>
                        </div>
                        <div className="product-button">
                            <Button label='Просмотр' style={{marginRight:'.25em', width: '50%'}} onClick={(e) => {
                                history.push('catalog/' + product.productId + '/preview')
                            }} />
                            <Button label='Редактирование' style={{marginRight:'.25em', width: '50%'}} onClick={(e) => {
                                history.push('catalog/' + product.productId + '/edit')
                            }} />
                            <SplitButton model={this.state.buttons} tabIndex={product}></SplitButton>
                        </div>
                    </Panel>
                </div>
            );
    }

    render(){

        const paginatorLeft = (<div>
            <span>{this.state.totalRows} результатов</span>
            <DataViewLayoutOptions layout={this.state.layout} onChange={(e) => this.setState({layout: e.value})} />
        </div>);

        const paginatorRight =(<div>
            <Button icon="pi pi-download" style={{marginRight:'.25em'}}/>
            <Button icon="pi pi-upload" onClick={() => console.log(Object.values(this.state.checked))} />
        </div>);

        const footer = (
            <div>
                <Button label={this.state.item.id?"Сохранить":"Добавить"} className={this.state.item.id?"":"p-button-success"} onClick={this.saveItem} />
                <Button label="Отменить" className="p-button-danger" onClick={this.closeDialog} />
            </div>
        );

        const header = (
            <div className="p-grid">
                <tr>
                    <th width='40vw'><Checkbox></Checkbox></th>
                    <th width='60vw'><span>ddd</span></th>
                    <th width='170px'><span>bbb</span></th>
                    <th width='170px'><span>nnn</span></th>
                    <th width='170px'><span>jjj</span></th>
                </tr>
            </div>
        );

        return (<div className='dataview-container'>
            <Toolbar>
                <div className="p-toolbar-group-left">
                    <BreadCrumb model={items} home={{label: 'AAA', icon: 'pi pi-home'}} />
                </div>
                <div className="p-toolbar-group-right">
                    <Button label='Добавить' className="p-button-success" onClick={e => console.log(this.state.checked)} />
                </div>
            </Toolbar>
            <DataView value={this.state.items}
                       onRowDoubleClick={this.onSelect}
                       layout={this.state.layout}
                      itemTemplate={this.itemTemplate}
                       scrollable={true} scrollHeight='100%'
                       currentPageReportTemplate={''} paginatorRight={paginatorRight} paginatorLeft={paginatorLeft}
                       selection={this.state.selectedItems} onSelectionChange={e => this.setState({selectedItems: e.value})}
                       paginator rows={20} paginatorPosition={'top'}
                      paginatorTemplate="CurrentPageReport FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink RowsPerPageDropdown" rowsPerPageOptions={[20,40,60,80,100]}
                      />
        </div>)
    }
}